export class Game {

  constructor() {
    console.log('Game instantiated.');
  }

  foo(message) {
    console.log('foo!');
    let test = 'es8'.padStart(10, 'x');
    console.log(test);
  }

}

window.addEventListener('DOMContentLoaded', () => {

  let game = new Game();
  game.foo();

});
